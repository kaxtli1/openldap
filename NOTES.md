
## instantiate openldap

Do not edit slapd.conf it's not used. 
/etc/ldap/slapd.d/

The initial setup of openldap is a chiken-egg problem. OpenLDAP is a database of users and need a user accont to access the users.
- We need a admin user to add entities to the tree. But the admin user is a leaf of the tree.
- The tree is not defined because is organization dependient.
- But the admin user can't be added because there aren't admin user to add entities.


To solve this problem, the admin password is stored in the **configuration setting**. 

The configuration of openldap is stored in openldap itself. And can be accessed from localhost witout password.

It is procces to create the admin user is:

- add a password to the admin user in configuration settings.
- Use the admin password to cretate the tree
- Add the admin user to the tree.

NOTE: maybe slapadd can be used too to solve the chiken-egg problem. Because it have direcct access to the database. And it's used to create the configuration entries, and the database.




In Debian by default, the noninteractive installation set a random password and a 'nodomain' domain name.
This can viwed by:

    ldapsearch -x -b '' -s base '(objectclass=*)' namingContexts


This are the attributes of interes in the **database backend**

    dn: olcDatabase={1}mdb
    olcSuffix: dc=nodomain
    olcRootDN: cn=admin,dc=nodomain
    olcRootPW:: e1NTSEF9aGJsdERQZ2Y4S1gyMW45Wm1ZWmpSVytmQVFCajFHakw=

This can be found in the database file /etc/ldap/slapd.d/cn=config/olcDatabase={1}mdb.ldif


    cat '/etc/ldap/slapd.d/cn=config/olcDatabase={1}mdb.ldif'
    # AUTO-GENERATED FILE - DO NOT EDIT!! Use ldapmodify.
    # CRC32 eb87a808
    dn: olcDatabase={1}mdb
    objectClass: olcDatabaseConfig
    objectClass: olcMdbConfig
    olcDatabase: {1}mdb
    olcDbDirectory: /var/lib/ldap
    olcSuffix: dc=nodomain
    olcAccess: {0}to attrs=userPassword by self write by anonymous auth by * none
    olcAccess: {1}to attrs=shadowLastChange by self write by * read
    olcAccess: {2}to * by * read
    olcLastMod: TRUE
    olcRootDN: cn=admin,dc=nodomain
    olcRootPW:: e1NTSEF9aGJsdERQZ2Y4S1gyMW45Wm1ZWmpSVytmQVFCajFHakw=
    olcDbCheckpoint: 512 30
    olcDbIndex: objectClass eq
    olcDbIndex: cn,uid eq
    olcDbIndex: uidNumber,gidNumber eq
    olcDbIndex: member,memberUid eq
    olcDbMaxSize: 1073741824
    structuralObjectClass: olcMdbConfig
    entryUUID: b47c7278-aaa6-103d-921e-b707014a00d6
    creatorsName: cn=admin,cn=config
    createTimestamp: 20230629085722Z
    entryCSN: 20230629085722.369769Z#000000#000#000000
    modifiersName: cn=admin,cn=config
    modifyTimestamp: 20230629085722Z


To change, it must be used the ldapmodify utilitie:


    root@26e708eed160:~# ldapmodify -Y EXTERNAL -H ldapi:///  <<EOF         
    dn: olcDatabase={1}mdb,cn=config
    changetype: modify
    replace: olcSuffix
    olcSuffix: dc=aula,dc=red


    dn: olcDatabase={1}mdb,cn=config
    changetype: modify
    replace: olcRootDN
    olcRootDN: cn=admin,dc=aula,dc=red


    dn: olcDatabase={1}mdb,cn=config
    changetype: modify
    replace: olcRootPW
    olcRootPW: abc
    EOF



Altenativy, the slapmodify can be used. It needs than the slapd be down. 
 It's better for a initContainer


    slapmodify -l config.ldiff -n 0

**This cretate a admin password in the database even the admin user doesn't exist**

So then, we can add any entities:


    org.ldiff

    dn: dc=aula,dc=red
    objectclass: dcObject
    objectclass: organization
    dc: aula 
    o: Example Company

    dn: cn=admin,dc=aula,dc=red
    objectclass: organizationalRole
    cn: admin


if the dn: is dc=aula,dc=red the  dc: must be aula. That is, the first DC. 

And use the admin password to add them

    ldapadd -x -D "cn=admin,dc=aula,dc=red" -w abc  -H ldap:// -f org.ldiff


    ldapsearch -x -D "cn=admin,dc=aula,dc=red" -w abc -b "dc=aula,dc=red"

the cn=config root entry contains global settings 
"OpenLDAP supports a number of different database backends, the default one being MDB and the alternatives slated for removal. Unless you have specific needs, like running a legacy installation, you should stick with the default. "




#### The config entries.

OpenLDAP stores his configuration in openldap itself. 
Thats means that it have **the initial database backend**  (dn: olcDatabase={0}config,cn=config )
The inital configuracion is stored using slapadd.
After that, the database number 1 is created ( with sladadd ) ( dn: olcDatabase={1}mdb,cn=config )
In this database the **olcSufix** attribute specifiy that **all queries and updates to this sufix is managed in this database**
You can create another database, and define a distinct sufix, to store all entities is this other database.


The whole configuration can be dumped with 

    ldapsearch -LLLQ -Y EXTERNAL -H ldapi:/// -b cn=config 

    ldapsearch -LLLQ -Y EXTERNAL -H ldapi:/// -b cn=config dn
dn: cn=config




### Openldap organization


OpenLDAP is organized in databases. The database number 0 stores the configuration. 
This database is stored ( I think ) in __/etc/ldap/slapd.d/__



	slapcat -n 0



The database number 1 is for store the user data. 
Usualy each openldap instance only uses the database 1 in production.
This database is stored in __/var/lib/ldap/__


	slapcat -n 1

## initContainer

In an initContainer the slapmodify and slapadd commands can be used, because not need a slapd server runing.



	# edit config /etc/ldap/conf.d/
	echo "
		dn: olcDatabase={1}mdb,cn=config
		changetype: modify
		replace: olcSuffix
		olcSuffix: dc=aula,dc=red

		dn: olcDatabase={1}mdb,cn=config
		changetype: modify
		replace: olcRootDN
		olcRootDN: cn=admin,dc=aula,dc=red

		dn: olcDatabase={1}mdb,cn=config
		changetype: modify
		replace: olcRootPW
		olcRootPW: abc
	" |  sed 's/^ *//' | slapmodify -n 0

	# edit data tree /var/lib/ldap
	echo "
	    dn: dc=aula,dc=red
	    objectclass: dcObject
	    objectclass: organization
	    dc: aula 
	    o: Example Company

	    dn: cn=admin,dc=aula,dc=red
	    objectclass: organizationalRole
	    cn: admin
	" |  sed 's/^ *//' | slapadd -n 1


Must to be ejecuted by openldap:openldap user, if not:

	 chown openldap:openldap /etc/ldap/slapd.d/ -R

## Debian config


kaxtli try to maintain close compatibility with debian. So:

- /usr/local/bin 		contain **setup.sh** script for the initContainer
- /etc/ldap/container/	templates that **setup.sh** uses


The initContainer use setup

	cmd: /etc/local/bin/setup
	args: ["PASSWORD"]


## ldapscripts

You can manage users easy with:

    apt install ldapscripts


## PAM LDAP

It seems like it mandatory to use the **nslcd** daemon LDAP client, to perform LDAP queries to a server.

So, 

    registry.conexo.mx/kaxtli/debian:bookworm-pam-ldap

    apt install -y  --no-install-recommends libpam-ldap


    sed 's/systemd/systmed ldap/' -i /etc/nsswitch.conf


    echo "session    required    pam_mkhomedir.so" >> /etc/pam.d/login


    sed 's/session *required *pam_loginuid.so/#session    requiered    pom_loginuid.so/' -i /etc/pam.d/login 


    getent passwd
    getent shadows



dn: ou=people,dc=aula,dc=red
objectClass: organizationalUnit
ou: people

dn: ou=groups,dc=aula,dc=red
objectClass: organizationalUnit
ou: groups


# Usuario
dn: uid=oscar,ou=people,dc=aula,dc=red
objectClass: posixAccount
cn: Oscar
homeDirectory: /home/oscar
uidNumber: 3000
gidNumber: 3000
objectClass: shadowAccount
objectClass: inetOrgPerson
userPassword: SSHA}Col97XEWo+S0PqUwnu5MiW3Rjo9B9egM
objectClass: person
sn: Pérez

# Grupo
dn: cn=oscar2,ou=groups,dc=aula,dc=red
objectClass: posixGroup
cn: oscar
gidNumber: 3000
memberUid: oscar



ldapadd -x -D cn=admin,dc=aula,dc=red -W -f quiron.ldif 

	# List all users
	ldapsearch -x -LLL -H ldap://openldap-pod  -D cn=admin,dc=aula,dc=red -b ou=people,dc=aula,dc=red -w 123456

nslcd


login oscar
