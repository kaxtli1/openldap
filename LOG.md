## Instantiation

By default, there are no password in the debian nointeractive installation. The password is in the olcRootPW attribute in:

    cat /etc/ldap/slapd.d/cn=config/olcDatabase={0}config.ldif


To set the initial password for the user cn=admin,cn=config

    pass.ldiff
        dn: olcDatabase={0}config,cn=config
        changetype: modify
        replace: olcRootPW
        olcRootPW: 123

    ldapmodify -Y EXTERNAL -H ldapi:/// -f pass.ldiff


This can be done interactive:

    root@debian:~# ldapmodify -Y EXTERNAL -H ldapi:/// 

    SASL/EXTERNAL authentication started
    SASL username: gidNumber=0+uidNumber=0,cn=peercred,cn=external,cn=auth
    SASL SSF: 0

    dn: olcDatabase={0}config,cn=config
    changetype: modify
    replace: olcRootPW
    olcRootPW: abc

    modifying entry "olcDatabase={0}config,cn=config"




After this, the olcRootPW attribute has the cypher password:

    # AUTO-GENERATED FILE - DO NOT EDIT!! Use ldapmodify.
    # CRC32 94563096
    dn: olcDatabase={0}config
    objectClass: olcDatabaseConfig
    olcDatabase: {0}config
    olcAccess: {0}to * by dn.exact=gidNumber=0+uidNumber=0,cn=peercred,cn=external
     ,cn=auth manage by * break
    olcRootDN: cn=admin,cn=config
    structuralObjectClass: olcDatabaseConfig
    entryUUID: b47be83a-aaa6-103d-9217-b707014a00d6
    creatorsName: cn=config
    createTimestamp: 20230629085722Z
    olcRootPW:: MTIz
    entryCSN: 20230629094216.711172Z#000000#000#000000
    modifiersName: gidNumber=0+uidNumber=0,cn=peercred,cn=external,cn=auth
    modifyTimestamp: 20230629094216Z


I think so that this show the entire data base


    ldapsearch -x -b '' -s base '(objectclass=*)'


the ldap.conf


- BASE <base> Specifies the default base DN to use when performing ldap operations. The base must be specified as a Distinguished Name in LDAP format. 


To show the nameingContext:

    ldapsearch -x -b '' -s base '(objectclass=*)' namingContexts


There are no naming context after installation.


    ldapmodify -Y EXTERNAL -H ldapi:///  <<EOF         
    dn: olcDatabase={1}mdb,cn=config
    changetype: modify
    replace: olcSuffix
    olcSuffix: dc=aula,dc=red


    dn: olcDatabase={1}mdb,cn=config
    changetype: modify
    replace: olcRootDN
    olcRootDN: cn=admin,dc=aula,dc=red


    dn: olcDatabase={1}mdb,cn=config
    changetype: modify
    replace: olcRootPW
    olcRootPW: abc
    EOF


dump the entire database. Maybe with ldapcat

    ldapcat


ldapsearchs

    -x      simple authentication
    -b      base subtree of the search
    -s      scope: base (the node itsefl) one ( onelevel: the direct childrens ) subtree (default) 




### Configuration - Dababase backend and password


NOTE: for command line, this password works fine. But, for dovecot need to be {SSHA} at least.

https://www.openldap.org/doc/admin26/dbtools.html


You need to configure slapd so that you can connect to it as a directory user with permission to add entries. You can configure the directory to support a special super-user or root user just for this purpose. This is done through the following two options in the database definition:

        rootdn <dn>
        rootpw <passwd>

For example:

        rootdn "cn=Manager,dc=example,dc=com"
        rootpw secret

These options specify a DN and password that can be used to authenticate as the super-user entry of the database (i.e., the entry allowed to do anything). The DN and password specified here will always work, regardless of whether **the entry named actually exists or has the password given**. This solves the **chicken-and-egg problem** of how to authenticate and add entries before any entries yet exist.

** The configuration on openldap is stored in 'openldap' itself**

https://www.openldap.org/doc/admin26/slapdconf2.html


__Once the software has been built and installed, you are ready to configure slapd(8) for use at your site.

OpenLDAP 2.3 and later have transitioned to using a dynamic runtime configuration engine, slapd-config(5). slapd-config(5)

    is fully LDAP-enabled
    is managed using the standard LDAP operations
    stores its configuration data in an LDIF database, generally in the /usr/local/etc/openldap/slapd.d directory.
    allows all of slapd's configuration options to be changed on the fly, generally without requiring a server restart for the changes to take effect.
__



**The olcSuffix select the database backend for a subtree, but NOT DEFINE A SUBTREE**

Thats why can **define password to access the database** but, do not create an item olcRootDN: cn=Manager,dc=example,dc=com

5.2.5.7. olcSuffix: <dn suffix>

This directive specifies the DN suffix of queries that will be passed to this backend database. Multiple suffix lines can be given, and usually at least one is required for each database definition. (Some backend types, such as frontend and monitor use a hard-coded suffix which may not be overridden in the configuration.)

Example:

        olcSuffix: dc=example,dc=com

Queries with a DN ending in "dc=example,dc=com" will be passed to this backend.



5.2.5.4. olcRootDN: <DN>

This directive specifies the DN that is not subject to access control or administrative limit restrictions for operations on this database. The DN need not refer to an entry in this database or even in the directory. The DN may refer to a SASL identity.

## initContainer

In an initContainer the slapmodify and slapadd commands can be used, because not need a slapd server runing.



	# edit config /etc/ldap/conf.d/
	echo "
		dn: olcDatabase={1}mdb,cn=config
		changetype: modify
		replace: olcSuffix
		olcSuffix: dc=aula,dc=red

		dn: olcDatabase={1}mdb,cn=config
		changetype: modify
		replace: olcRootDN
		olcRootDN: cn=admin,dc=aula,dc=red

		dn: olcDatabase={1}mdb,cn=config
		changetype: modify
		replace: olcRootPW
		olcRootPW: abc
	" |  sed 's/^ *//' | slapmodify -n 0

	# edit data tree /var/lib/ldap
	echo "
	    dn: dc=aula,dc=red
	    objectclass: dcObject
	    objectclass: organization
	    dc: aula 
	    o: Example Company

	    dn: cn=admin,dc=aula,dc=red
	    objectclass: organizationalRole
	    cn: admin
	" |  sed 's/^ *//' | slapadd -n 1


## Memory usage


https://github.com/osixia/docker-openldap/blob/master/image/service/slapd/startup.sh

    # Reduce maximum number of number of open file descriptors to 1024
    # otherwise slapd consumes two orders of magnitude more of RAM
    # see https://github.com/docker/docker/issues/8231
    ulimit -n $LDAP_NOFILE



## Using  debconf-set-selections and dpkg or apt


https://github.com/osixia/docker-openldap/blob/master/image/service/slapd/startup.sh



	    get_ldap_base_dn
	    cat <<EOF | debconf-set-selections
	slapd slapd/internal/generated_adminpw password ${LDAP_ADMIN_PASSWORD}
	slapd slapd/internal/adminpw password ${LDAP_ADMIN_PASSWORD}
	slapd slapd/password2 password ${LDAP_ADMIN_PASSWORD}
	slapd slapd/password1 password ${LDAP_ADMIN_PASSWORD}
	slapd slapd/dump_database_destdir string /var/backups/slapd-VERSION
	slapd slapd/domain string ${LDAP_DOMAIN}
	slapd shared/organization string ${LDAP_ORGANISATION}
	slapd slapd/backend string ${LDAP_BACKEND^^}
	slapd slapd/purge_database boolean true
	slapd slapd/move_old_database boolean true
	slapd slapd/allow_ldap_v2 boolean false
	slapd slapd/no_configuration boolean false
	slapd slapd/dump_database select when needed
	EOF

	    dpkg-reconfigure -f noninteractive slapd

## error: Cannot make/remove an entry for the specified session 


2023-07-14

    https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=726661


    Workarround:

    sed 's/session *required *pam_loginuid.so/#session    requiered    pom_loginuid.so/' -i /etc/pam.d/login 



